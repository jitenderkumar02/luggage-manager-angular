import { Router } from '@angular/router';
import { Component, Input, Output, EventEmitter, NgModule} from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  constructor(private router: Router){
  }

  email: string;
  password: string;

  login() {
    if (this.email === 'admin' && this.password === 'password') {
      //alert('Login successful!');
      this.router.navigate(['/fligtDetails']);
    } else {
      alert('Login failed. Please check your credentials.');
    }
  }
}
