import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FlightdetailsComponent } from './flightdetails/flightdetails.component';
import { ResultComponent } from './result/result.component';
import { LoginComponent } from './login/login.component';
import { FinalitemlistComponent } from './finalitemlist/finalitemlist.component';


const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'fligtDetails', component: FlightdetailsComponent },
  { path: 'result', component: ResultComponent },
  { path: 'finalItems', component: FinalitemlistComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
