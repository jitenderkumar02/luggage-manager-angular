import { Component } from '@angular/core';
import { FlightService } from '../services/flight.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-flightdetails',
  templateUrl: './flightdetails.component.html',
  styleUrls: ['./flightdetails.component.css']
})
export class FlightdetailsComponent {

  airportArr: Array<any> = [];
  airlineArr: Array<any> = [];
  itemResult: Array<any> = [];
  videoSubmitted: boolean = false;

  selectedFromAirport: string;
  selectedToAirport: string;
  selectedAirline: string;
  flightDate: string;
  file: File;

  constructor(private flightService: FlightService, private router: Router) {

  }

  onKeyAirport(event: KeyboardEvent) {
    this.flightService.getAirportNames((<HTMLInputElement>event.target).value).subscribe(
      airportNames => {
        this.airportArr = <String[]>airportNames;
      }
    )
  }

  onKeyAirline(event: KeyboardEvent) {
    this.flightService.getAirlineNames((<HTMLInputElement>event.target).value).subscribe(
      result => {
        this.airlineArr = <String[]>result;
      }
    )
  }

  onChange(event: any) {
    this.file = event.target.files[0];
  }

  submitForm() {
    // alert('values are ' + this.selectedFromAirport + ' ' + this.selectedToAirport + ' ' +
    //   this.selectedAirline + ' ' + this.flightDate + ' ' + this.file.name);
    this.videoSubmitted = true;
    this.flightService.submitVideo(this.selectedToAirport, this.selectedAirline, this.file).subscribe(
      result => {
        this.videoSubmitted = false;
        this.itemResult = <String[]>result;
        this.flightService.setItemResult(this.itemResult);
        this.router.navigate(['/result']);
      }
    )
  }

  homePage() {
    this.router.navigate(['/fligtDetails']);
  }

  getMinDate(): string {
    const currentDate = new Date();
    const minDate = currentDate.getFullYear() + "-" + this.getMonth(currentDate) + "-" + currentDate.getDate();

    return minDate;
  }

  getMonth(currentDate: Date): string {
    const month = currentDate.getMonth() + 1;
    if (month < 10) {
      return '0' + month;
    }
    return '' + month;
  }

}
