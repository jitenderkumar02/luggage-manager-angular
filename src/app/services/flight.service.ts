import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FlightService {

  url = 'http://localhost:8080/';

  itemResult: Array<any> = [];

  constructor(private http: HttpClient) { }

  getAirportNames(seachString: string): Observable<String[]> {
    if (seachString.length >= 3) {
      return this.http.get<String[]>(this.url + 'fetchAirports?airportKeyword=' + seachString)
    } else {
      return of([]);
    }
  }

  getAirlineNames(airlineText: string): Observable<String[]> {
    if (airlineText.length >= 2) {
      return this.http.get<String[]>(this.url + 'fetchAirlines?airlineIATACodes=' + airlineText)
    } else {
      return of([]);
    }
  }

  submitVideo(selectedToAirport: string, selectedAirline: string, file: File): Observable<String[]> {

    let formData: FormData = new FormData();
    formData.append('video', file, file.name);

    return this.http.post<String[]>(this.url + 'processItems/?airport=' + selectedToAirport + '&airline=' + selectedAirline, formData);
  }

  setItemResult(reuslt: Array<any>) {
    this.itemResult = reuslt;
  }

  getItemResult(): Array<any> {
    return this.itemResult;
  }

}
