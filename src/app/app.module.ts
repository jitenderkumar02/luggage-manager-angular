import { NgModule } from '@angular/core';
import 'hammerjs';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatTooltipModule } from '@angular/material/tooltip';



import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { FlightdetailsComponent } from './flightdetails/flightdetails.component';
import { ResultComponent } from './result/result.component';
import { FinalitemlistComponent } from './finalitemlist/finalitemlist.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    FlightdetailsComponent,
    ResultComponent,
    FinalitemlistComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    MatProgressBarModule,
    MatTooltipModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
