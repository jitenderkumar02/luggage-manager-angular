import { Component } from '@angular/core';
import { FlightService } from '../services/flight.service';
import { Item } from '../item';
import { Router } from '@angular/router';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent {

  items: Array<Item> = [];
  finalItems: Array<Item> = [];
  item: Item;
  checkedItems: Array<string> = [];

  constructor(private flightService: FlightService, private router: Router) {
    this.items = this.flightService.getItemResult();
    this.finalItems = this.items.map(x => Object.assign({}, x));
  }

  getClass(code: string) {
    if (code == 'CB' || code == 'CHK' || code == 'ALL')
      return 'allowed badge';
    else (code == 'NA')
    return 'not-allowed badge';
  }

  homePage() {
    this.router.navigate(['/fligtDetails']);
  }

  saveItems() {
    this.items = this.items.filter(item => item.id !== 'DEFAULT' || this.checkedItems.includes(item.name));
    this.flightService.setItemResult(this.items);
    this.router.navigate(['/finalItems']);
  }

  modifyCheckBoxvalue(event: any, itemName: string) {
    if (event.srcElement.checked) {
      this.checkedItems.push(itemName);
    } else {
      this.checkedItems = this.checkedItems.filter(name => name !== itemName);
    }

  }

}
