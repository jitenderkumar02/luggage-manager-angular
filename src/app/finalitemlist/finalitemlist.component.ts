import { Component } from '@angular/core';
import { Item } from '../item';
import { Router } from '@angular/router';
import { FlightService } from '../services/flight.service';

@Component({
  selector: 'app-finalitemlist',
  templateUrl: './finalitemlist.component.html',
  styleUrls: ['./finalitemlist.component.css']
})
export class FinalitemlistComponent {
  items: Array<Item> = [];

  constructor(private flightService: FlightService, private router: Router) {
    this.items = this.flightService.getItemResult();
  }

  homePage() {
    this.router.navigate(['/fligtDetails']);
  }
}

