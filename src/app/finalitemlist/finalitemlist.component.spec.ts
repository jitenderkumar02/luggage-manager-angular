import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FinalitemlistComponent } from './finalitemlist.component';

describe('FinalitemlistComponent', () => {
  let component: FinalitemlistComponent;
  let fixture: ComponentFixture<FinalitemlistComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FinalitemlistComponent]
    });
    fixture = TestBed.createComponent(FinalitemlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
