export interface Item {
    id: string;
    name: string;
    confidence: number;
    code: string;
    type: string;
 }